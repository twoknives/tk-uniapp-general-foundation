import App from './App'

// #ifndef VUE3
import Vue from 'vue'
Vue.config.productionTip = false
App.mpType = 'app'
import uView from '@/uni_modules/uview-ui'
Vue.use(uView)

 //引入全局请求插件
import { http,api } from '@/config/common.js' // 全局挂载引入，配置相关在该index.js文件里修改
Vue.prototype.$http = http
Vue.prototype.$api = api
 
import store from './store'
Vue.prototype.$store = store;

import share from '@/config/share.js'
Vue.mixin(share)


const app = new Vue({
    ...App
})
app.$mount()
// #endif

// #ifdef VUE3
import { createSSRApp } from 'vue'
export function createApp() {
  const app = createSSRApp(App)
  return {
    app
  }
}
// #endif