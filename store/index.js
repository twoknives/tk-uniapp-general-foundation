import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
	state: {
		/**
		 * 是否需要强制登录
		 */
		forcedLogin: false,
		hasLogin: false,
		userType:'user_register',//用户类型, 
		invoices_id:'',//提交的发票id
		user_info:{
			'head_img':'../../static/img/headimg.jpg',
			'nickname':'点击登录',
		 },//用户资料
		 order_info_route:{},//订单线路信息
		 scan_to_bus_id:'',//扫码乘车的车辆id
		 scan_oid:'',//扫码乘车的订单ID
		 
		
	},
	mutations: {
		login(state, userName) {
			state.userName = userName || '新用户';
			state.hasLogin = true;
		},
		logout(state) {
			state.userName = "";
			state.hasLogin = false;
		},
		changeweburl(state,weburl) {
			state.weburl = weburl || "测试地址"; 
		},
		checklogin(){
		 
			console.log("checklogin");
		}
	}
})

export default store
