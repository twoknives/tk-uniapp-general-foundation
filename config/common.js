import Request from '@/js_sdk/luch-request/luch-request/index.js'

const apiBaseUrl = 'https://demoxcx.yssknet.com/api/xcx/'


const api = new Request()
const http = new Request()
export {
	http,
	api
}



api.setConfig((config) => {
	/* 设置全局配置 */
	config.baseURL = apiBaseUrl
	config.header = {
		...config.header,
		'event': 'user_register',
		// 'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'


	}
	config.custom = {
		testaaa: true
	}
	return config
})


api.interceptors.request.use((config) => { // 可使用async await 做异步操作
	config.baseURL = apiBaseUrl
	config.header = {
		...config.header,
		// 'testaaa': '11' // 演示拦截器header加参
	}
	const token = uni.getStorageSync('token');
	if (token) {
		// 如果token不存在，return Promise.reject(config) 会取消本次请求
		config.header['token'] = token
	}

	// 演示custom 用处
	// if (config.custom.auth) {
	//   config.header.token = 'token'
	// }
	// if (config.custom.loading) {
	//  uni.showLoading()
	// }
	/**
	 /* 演示
	 if (!token) { // 如果token不存在，return Promise.reject(config) 会取消本次请求
	    return Promise.reject(config)
	  }
	 **/
	return config
}, config => { // 可使用async await 做异步操作
	return Promise.reject(config)
})


// 请求后
api.interceptors.response.use((response) => {
	// console.log(response)
	return response
}, (response) => {
	/*  对响应错误做点什么 （statusCode !== 200）*/
	// console.log(response)
	// console.log(response.statusCode);

	if (response.statusCode == 401 || response.statusCode == 402) {
		uni.clearStorageSync();
		uni.showToast({
			title: "请先登录",
			icon: 'none'
		})

		setTimeout(function() {
			uni.navigateTo({
				url: "/pages/common/login"
			})
		}, 1000);
	}

	if (response.statusCode == 403) {
		uni.clearStorageSync();
		uni.showToast({
			title: "请先登录",
			icon: 'none'
		})

		setTimeout(function() {
			uni.navigateTo({
				url: "/pages/common/login"
			})
		}, 1000);
	}

	return Promise.reject(response)
})
